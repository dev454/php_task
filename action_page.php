<?php
use Spatie\Async\Pool;

$servername = "localhost";
$username = "admin";
$password = "admin";
$dbname = "worldcom";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection to database failed: " . $conn->connect_error);
} else {
    // echo "Connected to DB </br>";
}

function selectFromDB() {
    global $conn;
    $sql = "SELECT p.Place, p.Name, p.Longitude, p.Latitude 
            FROM Places p LEFT INNER JOIN zip_codes z ON p.Name = z.Name 
            WHERE z.ZIP = " . $_POST["zipInput"];
    $result = $conn->query($sql);
    
    if ($result !== false && $result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo "Place: " . $row["p.Place"]. ", Place name: " $row["p.Name"]. ", Longitude: " $row["p.Longitude"]. ", Latitude: " . $row["p.Latitude"]. "<br>";
        }
    }
}

function fetchAndSaveToDB() {
    $url = "http://api.zippopotam.us/" . urlencode($_POST["countryList"]). "/" . urlencode($_POST["zipInput"]);
    $response = file_get_contents($url);
    $response = json_decode($response, true);    
    $placesArray = $response["places"];
    if (count($placesArray) > 0) {
        foreach ($placesArray as $place) {
            $name = $place["place name"]. " - " . $place["state"]. " - " . $response["country"];
            $sql = "INSERT INTO Places 
                    VALUES (" . $place["place name"]. ", " . $name. ", " . $place["longitude"]. ", " . $place["latitude"]. ")";
            $conn->query($sql);
            $sql = "INSERT INTO zip_codes 
                    VALUES (" . $name. ", " . $response["country"]. ", " . $response["post code"]. ")";
            $conn->query($sql);
        }
        return 1;
    } else {
        echo "There was an error finding this place";
        return;
    }
}

$pool = Pool::create();
if (is_null(selectFromDB())) {
    $pool->add(fetchAndSaveToDB)
    ->then(selectFromDB)
    ->catch(function ($exception) {
        echo $exception;
    })
}

await($pool);


$conn->close();

?>